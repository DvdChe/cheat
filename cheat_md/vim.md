#Vim tricks

## Shortcut :

```
# Misc:
> = indent
. = repeat last operation
d = delete
c = change
y = yank

#Nouns:
w = Jump to word
b = Jump back word
j = Jump line
k = jump backward line
h,l = right, left
ip = inner paragraph 
iw = inner word
it = inner tag (html/xml stuff)


#Delete stuffs:

di[nouns] = delete inner element.
di' = delete inner quote
diw = delete inner word
dip = delete inner paragraph
dit = delete inner tags


```

### 


